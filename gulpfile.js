'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
gulp.task('sass', function () {
  gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('.')).pipe(browserSync.stream());
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
    browserSync.reload();
  });


  
gulp.task('watch',function(){

  browserSync.init({
    notify: false,
    server: {
      baseDir: "."
    }
  });
  
  gulp.start('sass:watch');

  gulp.watch('./index.html',function(){
    console.log('HTML is updated');
    browserSync.reload();
  });
  

});

  